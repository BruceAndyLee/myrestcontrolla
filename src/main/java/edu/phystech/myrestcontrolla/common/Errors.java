package edu.phystech.myrestcontrolla.common;

public enum Errors {

    INVALID_FORM("<!DOCTYPE html>"+
            "<html>"+
            "<head>"+
            "    <meta charset=\"UTF-8\">" +
            "    <title>Title</title>" +
            "</head>"+
            "<body>"+
            "<p>form is invalid.</p>"+
            "<p>no additional info.</p>"+
            "<a href=\"http://127.0.0.1:8080/chats\">back to chats</a>"+
            "</body>"+
            "</html>"),

    OWNER_NOT_SET("<!DOCTYPE html>"+
                    "<html>"+
                    "<head>"+
                    "    <meta charset=\"UTF-8\">" +
                    "    <title>Error encountered</title>" +
                    "</head>"+
                    "<body>"+
                    "<p>Incomplete income data: owner is not set.</p>"+
                    "<p>no additional info.</p>"+
                    "<a href=\"http://127.0.0.1:8080/chats\">back to chats</a>"+
                    "</body>"+
                    "</html>"),

    CHAT_NAME_OCCUPIED("<!DOCTYPE html>"+
            "<html>"+
            "<head>"+
            "    <meta charset=\"UTF-8\">" +
            "    <title>Error encountered</title>" +
            "</head>"+
            "<body>"+
            "<p>This name is already occupied.</p>"+
            "<p>no additional info.</p>"+
            "<a href=\"http://127.0.0.1:8080/chats\">back to chats</a>"+
            "</body>"+
            "</html>"),

    CHAT_NOT_FOUND("<!DOCTYPE html>"+
            "<html>"+
            "<head>"+
            "    <meta charset=\"UTF-8\">" +
            "    <title>Error encountered.</title>" +
            "</head>"+
            "<body>"+
            "<p>No chat [by the name] found.</p>"+
            "<p>no additional info.</p>"+
            "<a href=\"http://127.0.0.1:8080/chats\">back to chats</a>"+
            "</body>"+
            "</html>"),

    MESSAGE_NOT_FOUND("<!DOCTYPE html>"+
            "<html>"+
            "<head>"+
            "    <meta charset=\"UTF-8\">" +
            "    <title>Error encountered.</title>" +
            "</head>"+
            "<body>"+
            "<p>There are either no messages, or the one you specified doesn't exist.</p>"+
            "<p>no additional info.</p>"+
            "</body>"+
            "</html>"),

    PERMISSION_DENIED("<!DOCTYPE html>"+
            "<html>"+
            "<head>"+
            "    <meta charset=\"UTF-8\">" +
            "    <title>Error encountered.</title>" +
            "</head>"+
            "<body>"+
            "<p>You don't have access to this chat, contact admin.</p>"+
            "<p>no additional info.</p>"+
            "<a href=\"http://127.0.0.1:8080/chats\">back to chats</a>"+
            "</body>"+
            "</html>"),

    OK("<!DOCTYPE html>"+
            "<html>"+
            "<head>"+
            "    <meta charset=\"UTF-8\">" +
            "    <title>Request fulfilled.</title>" +
            "</head>"+
            "<body>"+
            "<p>no additional info.</p>"+
            "</body>"+
            "</html>");


    private String htmlString;

    Errors(String s) {
        this.htmlString = s;
    }


    public String getHtmlString() {
        return this.htmlString;
    }

    public String getHtmlString(String replacement) {
        return this.htmlString.replace("no additional info.", replacement);
    }

}