package edu.phystech.myrestcontrolla.common;

public enum htmlTemplates {

    INDEX("<!DOCTYPE html>"+
            "<html>"+
            "<head>"+
            "    <meta charset=\"UTF-8\">" +
            "    <title>INDEX</title>" +
            "</head>"+
            "<body>"+
            "<h1>This is the default page.</h1>"+
            "</body>"+
            "</html>"),

    CHAT_TEMPL("<!DOCTYPE html>"+
            "<html>"+
            "<head>"+
            "    <meta charset=\"UTF-8\">"+
            "<body>" +
            "<a href=\"/participantaddition\">add participant</a><br>" +
            "##messages_table##" +
            "<form action=\"http://127.0.0.1:8080/postmessage\" method=\"post\">" +
            "<table>" +
            "<tr><th><input type=\"text\" name=\"chatName\" value=\"##chat_name##\"/></th></tr>" +
            "<tr><th><input type=\"text\" name=\"senderName\" placeholder=\"senderName\"/></th></tr>" +
            "<tr><th><input type=\"text\" name=\"text\" placeholder=\"text...\"></th></tr>" +
            "<tr><th><input type=\"submit\" id=\"send\" value=\"send\"/></th></tr>" +
            "</table>" +
            "</form>" +
            "<p><a href=\"http://127.0.0.1:8080/chats\">Back to chats</p>" +
            "</body>" +
            "</html>"),

    CHATS_LIST_TEMPL("<!DOCTYPE html>"+
            "<html>"+
            "<head>"+
            "    <meta charset=\"UTF-8\">" +
            "    <title>Title</title>" +
            "</head>"+
            "<body>"+
            "<h1>chats</h1>"+
            "##chats_table##"+
            "<table><a href=\"http://127.0.0.1:8080/chatcreationrequest\">" +
                "create chat" +
            "</a></table>" +
            "</body></html>"),

    CREATE_CHAT("<!DOCTYPE html>"+
            "<html>"+
            "<head>"+
            "    <meta charset=\"UTF-8\">" +
            "</head>" +
            "<body>" +
            "<form action=\"createchat\" method=\"post\">" +
            "<table>" +
            "<tr><th>" +
                "<input type=\"text\" name=\"owner\" placeholder=\"owner\">" +
            "</th></tr>" +
            "<tr><th>" +
                "<input type=\"text\" name=\"name\" placeholder=\"chat name\">" +
            "</th></tr>" +
            "<tr><th>" +
                "<input type=\"password\" name=\"password\" placeholder=\"password\">" +
            "</th></tr>" +
            "<tr><th>" +
                    "<input type=\"submit\" name=\"submit\" value=\"create\">" +
            "</th></tr>" +
            "</table>" +
            "</form>" +
            "</body>" +
            "</html>"),

    ADD_PARTICIPANT("<!DOCTYPE html>"+
            "<html>"+
            "<head>"+
            "    <meta charset=\"UTF-8\">" +
            "</head>" +
            "<body>" +
            "<form action=\"/addparticipant\" method=post>" +
            "<table>" +
            "<tr><th>" +
                "<input type=\"text\" name=\"chatOwner\" placeholder=\"owner\">" +
            "</th></tr>" +
            "<tr><th>" +
                "<input type=\"text\" name=\"chatName\" placeholder=\"chat name\">" +
            "</th></tr>" +
            "<tr><th>" +
                "<input type=\"password\" name=\"password\" placeholder=\"password\">" +
            "</th></tr>" +
            "<tr><th>" +
                "<input type=\"text\" name=\"participant\" placeholder=\"participant name\">" +
            "</th></tr>" +
            "<tr><th>" +
                "<input type=\"submit\" name=\"submit\" value=\"add\">" +
            "</th></tr>" +
            "</table>" +
            "</form>" +
            "</body>" +
            "</html>"
            );

    private String str;

    htmlTemplates(String s) {
        this.str = s;
    }

    public String getHTMLString() {
        return this.str;
    }

    public String getChatPage(String chatName, String messagesTable) {
        return this.str.replace("##chat_name##", chatName)
                    .replace("##messages_table##", messagesTable);
    }

    public String getChatsListPage(String chatsTable) {
        return this.str.replace("##chats_table##", chatsTable);
    }
}
