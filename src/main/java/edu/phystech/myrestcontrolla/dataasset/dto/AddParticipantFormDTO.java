package edu.phystech.myrestcontrolla.dataasset.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AddParticipantFormDTO {
    private String chatOwner;
    private String chatName;
    private String password;
    private String participant;

    public boolean isValid() {
        return participant != null
                && chatName != null
                && chatOwner != null
                && password != null;
    }
}
