package edu.phystech.myrestcontrolla.dataasset.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CreateChatFormDTO {
    private String owner;
    private String name;
    private String password;

    public boolean isValid() {
        return name != null && password != null && owner != null;
    }
}
