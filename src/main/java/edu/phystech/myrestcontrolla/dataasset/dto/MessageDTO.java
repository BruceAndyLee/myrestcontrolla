package edu.phystech.myrestcontrolla.dataasset.dto;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MessageDTO {
    private Integer id;
    private String chatName;
    private String senderName;
    private String text;
    private String time;

    public boolean isValid() {
        return null != chatName
                && null != senderName
                && null != text;
    }
}
