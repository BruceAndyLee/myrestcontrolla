package edu.phystech.myrestcontrolla.dataasset.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProfileDTO {
    private Integer id;
    private String name;
    private String password;
}
