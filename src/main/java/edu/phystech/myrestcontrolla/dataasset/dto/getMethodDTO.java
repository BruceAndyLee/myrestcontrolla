package edu.phystech.myrestcontrolla.dataasset.dto;

import java.math.BigInteger;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class getMethodDTO {

    private BigInteger mode;
    private BigInteger number;

    public getMethodDTO() {
    }
}
