package edu.phystech.myrestcontrolla.dataasset.dto;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class postMethodDTO{

    private String body;

    public postMethodDTO() {
    }

    public String insertString(String str) {
        if (body.isEmpty()) {
            return body;
        }
        return body.replaceAll("##user_name##", str);
    }

}
