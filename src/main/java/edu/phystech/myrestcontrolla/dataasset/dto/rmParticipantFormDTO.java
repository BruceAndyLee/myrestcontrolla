package edu.phystech.myrestcontrolla.dataasset.dto;


import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class rmParticipantFormDTO {

    private String chatOwner;
    private String chatName;
    private String participant;

    public boolean isValid() {
        return participant != null && chatName != null && chatOwner != null;
    }
}
