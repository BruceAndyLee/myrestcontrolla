package edu.phystech.myrestcontrolla.dataasset.pojo;

import edu.phystech.myrestcontrolla.dataasset.dto.MessageDTO;
import edu.phystech.myrestcontrolla.dataasset.dto.ProfileDTO;
import lombok.Getter;
import lombok.Setter;


import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


@Setter
@Getter
public class ChatPOJO {
    private ProfileDTO owner;
    private String name;
    private List<MessageDTO> messages;
    private List<String> participants;

    public  ChatPOJO() {
        this.messages = new ArrayList<>();
        this.name = "jam";
        this.owner = new ProfileDTO();
        this.participants = new ArrayList<>();
    }

    public String getInfo() {
        return  name + "; owned by " + owner.getName();
    }

    public String getMessagesFancy() {
        if (messages.isEmpty()) {
            return "No messages in this chat yet...";
        }
        StringBuilder result = new StringBuilder();
        List<MessageDTO> subList = messages.subList(messages.size()-11, messages.size()-1);
        for (MessageDTO message : subList){
            result.append(message.getTime())
                    .append(" ")
                    .append(message.getSenderName())
                    .append(" >> ")
                    .append(message.getText())
                    .append("\n");
        }
        return result.toString();
    }

    public String getMessagesHtmlTable() {
        if (messages.isEmpty()) {

            return "No messages in this chat yet...";
        }

        StringBuilder result = new StringBuilder();
        result.append("<table>");
        for (MessageDTO message : messages){
//            StringBuilder editMsgHref = new StringBuilder();
//            editMsgHref.append("<a href=\"http:/127.0.0.1:8080/editmessage?chatName=")
//                        .append(message.getChatName())
//                        .append("&messageId=")
//                        .append(message.getId())
//                        .append("&msg=")
//                        .append("");
            result.append("<tr><th>")
                    .append(message.getTime())
                    .append(" ")
                    .append(message.getSenderName())
                    .append(" >> ")
                    .append(message.getText())
                    .append("</th></tr>");
        }
        result.append("</table>");
        return result.toString();
    }

    public Integer postMessage(MessageDTO message) {
        message.setId(messages.size());
        message.setTime(new SimpleDateFormat("hh:mm").format(new Date()));
        messages.add(message);
        return messages.indexOf(message);
    }

    public List<MessageDTO> editMessage(Integer id, String text) {

        if (messages.isEmpty()) {
            return null;
        }

        if (id < 0 || id > messages.size()) {
            return null;
        }

        MessageDTO newMessage = new MessageDTO();
        newMessage.setText(text + "  (edited)");
        newMessage.setSenderName(messages.get(id).getSenderName());
        newMessage.setTime(new SimpleDateFormat("hh:mm").format(new Date()));
        messages.set(id, newMessage);
        return messages;
    }

    public void addParticipant(String participant) {
        if (participants.indexOf(participant) != -1) {
            return;
        }
        participants.add(participant);
    }

    public void removeParticipant(String participant) {
        if (participants.indexOf(participant) == -1) {
            return;
        }
        participants.remove(participant);
    }
}
