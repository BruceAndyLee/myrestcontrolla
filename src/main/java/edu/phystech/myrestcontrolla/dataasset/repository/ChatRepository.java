package edu.phystech.myrestcontrolla.dataasset.repository;

import java.math.BigDecimal;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.sql.DataSource;

import edu.phystech.myrestcontrolla.dataasset.dto.MessageDTO;
import edu.phystech.myrestcontrolla.dataasset.dto.ProfileDTO;
import edu.phystech.myrestcontrolla.dataasset.pojo.ChatPOJO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;


@Repository
public class ChatRepository extends JdbcDaoSupport {
    private DataSource dataSource;

    @Autowired
    public ChatRepository (DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @PostConstruct
    private void init() {
        setDataSource(dataSource);

        getJdbcTemplate().execute("drop table if exists chats");
        getJdbcTemplate().execute("drop table if exists users");
        getJdbcTemplate().execute("create table users (id serial, name varchar(50))");
        getJdbcTemplate().execute("create table chats (id serial, name varchar(50), owner_id serial)");
        //getJdbcTemplate().execute("create table messages" +
        //        "(id serial, chatName varchar(50), senderName varchar(50), text varchar(200), time varchar(20))");
        getJdbcTemplate().execute("insert into users (id, name) values (1, 'admin_1')");
        getJdbcTemplate().execute("insert into users (id, name) values (2, 'admin_2')");
        getJdbcTemplate().execute("insert into chats (id, name, owner_id) values (1, 'chat_1', 2)");
        getJdbcTemplate().execute("insert into chats (id, name, owner_id) values (2, 'chat_2', 2)");
    }

    public void createChat(String name, Integer owner_id) {
        String sql = "insert into chats (name, owner) values (?,?)";
        getJdbcTemplate().update(sql, name, owner_id);
    }

    public void addMessage(MessageDTO message) {
        String sql = "insert into messages (chatName, senderName, text, time)" +
                        " values (?, ?, ?, ? )";
        getJdbcTemplate().update( sql,
                message.getChatName(),
                message.getSenderName(),
                message.getText(),
                message.getTime());
    }

    public Map<Integer, ChatPOJO> getChats() {
        return getJdbcTemplate().query("select name, owner from chats",
        new ResultSetExtractor<Map<Integer, ChatPOJO>>() {
            @Override
            public Map<Integer, ChatPOJO> extractData(ResultSet resultSet) throws SQLException, DataAccessException {
                Map<Integer, ChatPOJO> chats = new HashMap<>();

                while(resultSet.next()) {
                    ChatPOJO chat = new ChatPOJO();

                    chat.setName(resultSet.getString(1));
                    ProfileDTO admin = new ProfileDTO();
                    admin.setName(resultSet.getString(2));
                    chat.setOwner(admin);
                    chats.put(chat.getName().hashCode(), chat);
                }

                return chats;
            }
        });
    }

    public List<MessageDTO> getChatMessages(String name) {
        return getJdbcTemplate().query("select * messages where messages.chatName = ?",
                new Object[]{name},
                new ResultSetExtractor<List<MessageDTO>>() {
                    @Override
                    public List<MessageDTO> extractData(ResultSet resultSet) throws SQLException, DataAccessException {

                        List<MessageDTO> resultList = new ArrayList<>();

                        while(resultSet.next()) {
                            MessageDTO message = new MessageDTO();
                            message.setChatName(resultSet.getString(1));
                            message.setSenderName(resultSet.getString(2));
                            message.setText(resultSet.getString(3));
                            message.setTime(resultSet.getString(4));

                            resultList.add(message);
                        }

                        return resultList;
                    }
                }
        );

    }

}
