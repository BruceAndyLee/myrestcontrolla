package edu.phystech.myrestcontrolla.rest;

import edu.phystech.myrestcontrolla.common.htmlTemplates;
import edu.phystech.myrestcontrolla.dataasset.dto.AddParticipantFormDTO;
import edu.phystech.myrestcontrolla.dataasset.dto.CreateChatFormDTO;
import edu.phystech.myrestcontrolla.dataasset.dto.rmParticipantFormDTO;
import edu.phystech.myrestcontrolla.service.ChatService;
import edu.phystech.myrestcontrolla.dataasset.dto.MessageDTO;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
public class ChatController {

    @Autowired
    private ChatService chatService;


    @GetMapping(path="/")
    public String index() {

        return htmlTemplates.INDEX.getHTMLString();
    }

    @GetMapping(path="/chats")
    public String getChats() {

        return chatService.getChats();
    }

    @GetMapping(path="/chatcreationrequest")
    public String getChatCreationForm(){

        System.out.println("I am in kkreation request");
        return htmlTemplates.CREATE_CHAT.getHTMLString();
    }

    @PostMapping(path = "/createchat",
            consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    public String createChat(CreateChatFormDTO form) {
        return chatService.createChat(form);
    }

    @RequestMapping(value = "/chat/messages", method = RequestMethod.GET)
    public String getChatMessages(@RequestParam String chatName, @RequestParam String username) {

        return chatService.getChatMessages(chatName, username);
    }

    @PostMapping(path="/postmessage",
            consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    public String postMessage(MessageDTO msg) {
        System.out.println(msg.getSenderName() + ": " + msg.getText() + "; into " + msg.getChatName());
        return chatService.postMessage(msg);
    }

    @PostMapping(value = "/editmessage")
    public String editMessage(@RequestParam String chatName,
                              @RequestParam Integer messageId,
                              @RequestParam String msg) {

        return chatService.editMessage(chatName, messageId, msg);
    }

    @GetMapping(path="/participantaddition")
    public String getAddParticipantForm() {

        return htmlTemplates.ADD_PARTICIPANT.getHTMLString();
    }

    @PostMapping(value = "/addparticipant",
            consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    public String addParticipant(AddParticipantFormDTO form) {
        return chatService.addParticipant(form);
    }

    @PostMapping(value = "/removeparticipant",
            consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    public ResponseEntity removeParticipant(rmParticipantFormDTO form) {
        return chatService.removeParticipant(form);
    }
}
