package edu.phystech.myrestcontrolla.rest;

import edu.phystech.myrestcontrolla.dataasset.dto.getMethodDTO;
import edu.phystech.myrestcontrolla.dataasset.dto.postMethodDTO;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.math.BigInteger;

@RestController
public class MyRestControlla {

    @RequestMapping(value = "/getBase", method = RequestMethod.GET)
    public ResponseEntity returnBase() {
        return ResponseEntity.ok().body("simplest request jam, hello there!");
    }

    @RequestMapping(value = "/getMode", method = RequestMethod.GET)
    public ResponseEntity getMode(@RequestBody getMethodDTO DTO, @RequestParam BigInteger number, @RequestParam BigInteger mode) {
        return ResponseEntity.ok().body(number.mod(mode));
    }

    @RequestMapping(value = "/modifyString", method = RequestMethod.POST)
    public ResponseEntity getMode(@RequestBody postMethodDTO DTO, @RequestParam String userName) {

        return ResponseEntity.ok().body(DTO.insertString(userName));
    }
}
