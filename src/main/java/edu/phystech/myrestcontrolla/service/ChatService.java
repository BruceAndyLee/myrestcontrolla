package edu.phystech.myrestcontrolla.service;

import edu.phystech.myrestcontrolla.common.Errors;
import edu.phystech.myrestcontrolla.common.htmlTemplates;
import edu.phystech.myrestcontrolla.dataasset.dto.*;
import edu.phystech.myrestcontrolla.dataasset.pojo.ChatPOJO;
import edu.phystech.myrestcontrolla.dataasset.repository.ChatRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class ChatService {

    @Autowired
    private ChatRepository repository;

    private Map<Integer, ChatPOJO> chats;

    public ChatService() {

        this.chats = new HashMap<>();
    }

    public String getChats() {

        //chats = repository.getChats();

        /*if (chats.isEmpty()) {
            return Errors.CHAT_NOT_FOUND.getHtmlString();
        }*/

        return getChatsHtml();
    }

    private String getChatsHtml() {
        StringBuilder chatsTable = new StringBuilder();
        chatsTable.append("<table>");
        if (chats.isEmpty()) {
             chatsTable.append("No chats have been created yet.");
        } else {
            Set chatKeys = chats.keySet();
            for (Object chatKey : chatKeys) {
                ChatPOJO chat = chats.get(chatKey);
                StringBuilder href = new StringBuilder();

                href.append("<a href=\"http://127.0.0.1:8080/chat/messages")
                        .append("?chatName=")
                        .append(chat.getName())
                        .append("&username=")
                        .append(chat.getOwner().getName())
                        .append("\">");

                chatsTable.append("<tr><th>" +
                        href.toString() +
                        chat.getInfo() +
                        "</a></th></tr>\n");
            }
        }
        return htmlTemplates.CHATS_LIST_TEMPL.getChatsListPage(chatsTable.toString());
    }

    public String createChat(CreateChatFormDTO form) {
        if (!form.isValid()) {
            return Errors.INVALID_FORM.getHtmlString();
        }
        if (form.getOwner().isEmpty()) {
            //return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Incomplete income data.");
            return Errors.OWNER_NOT_SET.getHtmlString();
        }
        ChatPOJO chat = getChatByName(form.getName());
        if (null != chat) {
            //return ResponseEntity.status(HttpStatus.CONFLICT).body("This name is already occupied.");
            return Errors.CHAT_NAME_OCCUPIED.getHtmlString();
        }

        ChatPOJO newChat = new ChatPOJO();
        newChat.setName(form.getName());
        newChat.addParticipant(form.getOwner());

        ProfileDTO admin = new ProfileDTO();
        admin.setName(form.getOwner());
        admin.setPassword(form.getPassword());
        newChat.setOwner(admin);
        chats.put(form.getName().hashCode(), newChat);

        return getChatsHtml();
    }

    public String getChatMessages(String chatName, String username) {

        ChatPOJO chat = getChatByName(chatName);
        if (null != chat) {

            if (chat.getParticipants().contains(username)) {
                //return ResponseEntity.ok(chat.getMessagesFancy());
                return htmlTemplates.CHAT_TEMPL
                        .getChatPage(chat.getName(), chat.getMessagesHtmlTable());
            }
            return Errors.PERMISSION_DENIED.getHtmlString();
        }

        return Errors.CHAT_NOT_FOUND.getHtmlString();
    }

    public String postMessage(MessageDTO message) {

        if (!message.isValid()) {
            return Errors.INVALID_FORM.getHtmlString();
        }
        ChatPOJO chat = getChatByName(message.getChatName());
        if (null != chat) {
            if (!chat.getParticipants().contains(message.getSenderName())) {
                return Errors.PERMISSION_DENIED.getHtmlString();
            }
            chat.postMessage(message);
            return getChatMessages(message.getChatName(), message.getSenderName());
        }

        return Errors.CHAT_NOT_FOUND.getHtmlString();
    }

    public String editMessage(String chatName, Integer messageId, String text) {

        ChatPOJO chat = getChatByName(chatName);
        if (null != chat) {

            if (chat.editMessage(messageId, text) != null) {
                return Errors.OK.getHtmlString(text + " --- set as new text");
            }
            return Errors.MESSAGE_NOT_FOUND.getHtmlString();
        }

        return Errors.CHAT_NOT_FOUND.getHtmlString();
    }

    private ChatPOJO getChatByName(String name) {

        Integer chat_id = name.hashCode();
        return chats.get(chat_id);
    }

    public String addParticipant(AddParticipantFormDTO form) {
        if (!form.isValid()) {
            return Errors.INVALID_FORM.getHtmlString();
        }
        if (!chats.containsKey(form.getChatName().hashCode())) {
            return Errors.CHAT_NOT_FOUND.getHtmlString();
        }

        ChatPOJO chat = chats.get(form.getChatName().hashCode());
        if (!chat.getOwner().getName().equals(form.getChatOwner())) {
            return Errors.PERMISSION_DENIED.getHtmlString("wrong admin name");
        }
        if (!chat.getOwner().getPassword().equals(form.getPassword())){
            return Errors.PERMISSION_DENIED.getHtmlString("wrong password");
        }

        chat.addParticipant(form.getParticipant());
        return getChatMessages(chat.getName(), form.getChatOwner());
    }

    public ResponseEntity removeParticipant(rmParticipantFormDTO form) {
        if (!form.isValid()) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Invalid form.");
        }
        if (!chats.containsKey(form.getChatName().hashCode())) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("No chat with the specified name.");
        }

        ChatPOJO chat = chats.get(form.getChatName().hashCode());
        if (!chat.getOwner().getName().equals(form.getChatOwner())) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("you don't have admin rights to add people.");
        }

        chat.removeParticipant(form.getParticipant());
        return ResponseEntity.ok().body(form.getParticipant() + " is (now) removed from the chat " + form.getChatName());

    }

}